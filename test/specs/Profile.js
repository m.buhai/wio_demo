let path = require('path');
class Login {
    constructor(loginName, pass) {
        this.loginName = loginName;
        this.pass = pass;
    }

    writeLogin() {
        let inputLogin = $('[name = "login"]')
        inputLogin.setValue(this.loginName)
    }

    writePass() {
        let inputPass = $('[name = "password"]')
        inputPass.setValue(this.pass)
    }
    clickLoginButton(){
        let loginButton = $('[type = "submit"]')
        loginButton.click()
    }
}
class ChangeAvatar{
    constructor() {

    }
    uploadAvatar(){
        let changeAvatarInput = $('input[type="file"]')
        changeAvatarInput.waitForDisplayed();
        let filePath = path.join(__dirname, '../image/newAvatar.png');
        let remoteFilePath = browser.uploadFile(filePath)
        changeAvatarInput.setValue(remoteFilePath)
        let uploadButtonSuccess = $('[class*="btn-success"]')
        uploadButtonSuccess.click()
    }

}
class FirstNameField {
    constructor() {
        this.textForInput = 'Тест'
        this.checker

    }

    writeFirstName(){

        let buttonFirstNameRus = $('button[editable-text="$ctrl.table.firstName.fieldValue"]')
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, 10000);
        let checker;
        checker = buttonFirstNameRus.getText()
        if (checker == 'Тест'){
            this.textForInput = 'Тестище'
        }
        buttonFirstNameRus.click()
        let inputFirstNameRus = $('[type="text"][class*="editable"]')
        inputFirstNameRus.setValue(this.textForInput)
        let okButtonFirstNameRus = $('[class*=\'glyphicon-ok\']')
        okButtonFirstNameRus.click()
    }
    checkFirstName(){
        let buttonFirstNameRus = $('button[editable-text="$ctrl.table.firstName.fieldValue"]')
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, 10000);
        expect(buttonFirstNameRus.getText()).toEqual(this.textForInput)
    }

    takeTextFromFirstNameField(){
        let buttonFirstNameRus = $('button[editable-text="$ctrl.table.firstName.fieldValue"]')
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, 10000);
        this.checker = buttonFirstNameRus.getText()
    }
    writeToLongText(){
        let buttonFirstNameRus = $('button[editable-text="$ctrl.table.firstName.fieldValue"]')
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, 10000);

        buttonFirstNameRus.click()
        let inputFirstNameRus = $('[type="text"][class*="editable"]')
        inputFirstNameRus.setValue('TV3DMms22uVqufK6UAu376kC7YpM9bnKIkwas4OzBbRpksziVLhJ392CBye6LDfAeNcwLmtJzW8LxRSnYcVvwJFMs2AtUZB0R5zDe81l7uvhfuQ9R6WlAhn12kvSt3tvrKkS4IJs2DC1PIqsjMqx1yBFKNwrKrj11tqhxVeoC3FsBpMr1BHxso26UUzlXx5YSpMbTViKbBEhhdgru4W2en8unIVJmFbPCiBv8FtBYirX8G8IsAFp2AGP0tGUSBgU')
        let okButtonFirstNameRus = $('[class*=\'glyphicon-ok\']')
        okButtonFirstNameRus.click()
    }
    checkErrorMessage(){
        let errorMessageField = $('[class*="editable-error"]')
        browser.waitUntil(function (){
            return errorMessageField.getText() != ""
        }, 10000);
        expect(errorMessageField.getText()).toEqual('Text length should be from 1 to 255 symbols. Symbol "-" and space are allowed. Only Russian symbols allowed.')
    }
    clickCancelButton(){
        let cancelButton = $('[title="Cancel"]')
        cancelButton.click()
    }
    checkTextinFirstNameRusField(){
        let buttonFirstNameRus = $('button[editable-text="$ctrl.table.firstName.fieldValue"]')
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, 10000);
        expect(buttonFirstNameRus.getText()).toEqual(this.checker)
    }
    makeFirstNameRusFieldEmpty(){
        let buttonFirstNameRus = $('button[editable-text="$ctrl.table.firstName.fieldValue"]')
        browser.waitUntil(function (){
            return buttonFirstNameRus.getText() != ""
        }, 10000);
        let checker = buttonFirstNameRus.getText()
        buttonFirstNameRus.click()
        let inputFirstNameRus = $('[type="text"][class*="editable"]')
        inputFirstNameRus.setValue('')
        let okButtonFirstNameRus = $('[class*=\'glyphicon-ok\']')
        okButtonFirstNameRus.click()
    }
}
class GenderField {
    constructor() {
        this.checking
    }
    changeGender(){
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        browser.waitUntil(function (){
            return genderButton.getText() != ""
        }, 10000);
        this.checking = genderButton.getText()
        genderButton.click()
        let genderSelect = $('[class*="editable-has-buttons"]')
        genderSelect.click()
        let choosingGender = $('[label="Male"]')
        if (this.checking == 'Male'){
            choosingGender = $('[label="Female"]')
        }
        this.checking = choosingGender.getText()
        choosingGender.click()
    }
    clickOkButton(){
        let okButtonFirstNameRus = $('[class*="glyphicon-ok"]')
        okButtonFirstNameRus.click()
    }
    checkGender(){
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        browser.waitUntil(function (){

            return genderButton.getText() != ""
        }, 10000);
        expect(genderButton.getText()).toEqual(this.checking)
    }
    takeGenderText(){
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        browser.waitUntil(function (){
            return genderButton.getText() != ""
        }, 10000);
        this.checking = genderButton.getText()
    }
    clickOnGenderWithoutChanging(){
        let genderButton = $('[editable-select="$ctrl.table.gender.fieldValue"]')
        genderButton.click()
        let cancelButton = $('[title="Cancel"]')
        cancelButton.click()
    }

}
class Shedule {
    openSheduleField(){
        let iconEditWorkShedule = $('[ng-if="$ctrl.isWorkScheduleManaged"]').$('[class*="icon-arrow-down"]')
        iconEditWorkShedule.click()
        iconEditWorkShedule.scrollIntoView()
    }
    setTimeFrom(){
        let timeFrom = $('#permanentWorkingHoursFrom')
        timeFrom.scrollIntoView()
        timeFrom.selectByAttribute('label', '07:00')
    }
    setTimeTill(){
        let timeTill = $('[ng-options*="end"]')
        timeTill.selectByAttribute('label', '10:00')
    }
    changePermanentWorkingTime(){
        let changePermanentWorkingTime = $('[ng-click="$ctrl.changePermanentWorkingTime()"]')
        changePermanentWorkingTime.click()
    }
    checkSuccessMessage(){
        let successMessageShow = $('[ng-if="$ctrl.data.successMessageShow"]')
        browser.waitUntil(function (){
            return successMessageShow.getText() != ""
        }, 10000);
        expect(successMessageShow.getText()).toEqual('×'+ '\n' +'Time was successfully changed.')
    }
}
let shedule = new Shedule()
let genderField = new GenderField()
let firstNameField = new FirstNameField()
let changeAvatar = new ChangeAvatar();
let login = new Login('manager', 'mobidevQA01!')

describe('test', () => {
    it('Login', () => {
        browser.url('http://52.212.87.108/')

        login.writeLogin();
        login.writePass();
        login.clickLoginButton()
        expect(browser).toHaveUrlContaining('http://52.212.87.108/profile/1')
    });

    it('Change Avatar', function () {
        browser.url('http://52.212.87.108/full-profile/1064')
        browser.waitUntil(function (){
            return $('[ng-model="$ctrl.avatar"]').getText() != ""
        }, 10000);
        browser.execute(function () {
            document.querySelector('label[style*="visibility: hidden"]').style = ' ';
        });

        changeAvatar.uploadAvatar()

    });

    it('Change First Name', function () {
        browser.url('http://52.212.87.108/full-profile/1064')

        firstNameField.writeFirstName()
        firstNameField.checkFirstName()
    });

    it('Change First Name with 256 symbols', function () {
        browser.url('http://52.212.87.108/full-profile/1064')

        firstNameField.takeTextFromFirstNameField()
        firstNameField.writeToLongText()
        firstNameField.checkErrorMessage()
        firstNameField.clickCancelButton()
        firstNameField.checkTextinFirstNameRusField()
    });

    it('Change First Name with empty field', function () {
        browser.url('http://52.212.87.108/full-profile/1064')

        firstNameField.takeTextFromFirstNameField()
        firstNameField.makeFirstNameRusFieldEmpty()
        firstNameField.checkErrorMessage()
        firstNameField.clickCancelButton()
        firstNameField.checkTextinFirstNameRusField()
    });

    it('Update gender', function () {
        browser.url('http://52.212.87.108/full-profile/1064')

        genderField.changeGender()
        genderField.clickOkButton()
        genderField.checkGender()
    });

    it('Cancel update gender', function () {
        browser.url('http://52.212.87.108/full-profile/1064')

        genderField.takeGenderText()
        genderField.clickOnGenderWithoutChanging()
        genderField.checkGender()
    });

    it('Choose Permanent shedule', function () {
        browser.url('http://52.212.87.108/profile/1')

        shedule.openSheduleField()
        shedule.setTimeFrom()
        shedule.setTimeTill()
        shedule.changePermanentWorkingTime()
        shedule.checkSuccessMessage()
    });

})