import allureReporter from '@wdio/allure-reporter'
class Login {
    constructor(loginName, pass) {
        this.loginName = loginName;
        this.pass = pass;
    }

    writeLogin() {
        let inputLogin = $('[name = "login"]')
        inputLogin.setValue(this.loginName)
    }

    writePass() {
        let inputPass = $('[name = "password"]')
        inputPass.setValue(this.pass)
    }
    clickLoginButton(){
        let loginButton = $('[type = "submit"]')
        loginButton.click()
    }
}



describe('test', () => {
    it('Login', () => {
        browser.url('http://52.212.87.108/')
        allureReporter.addStep('Enter the page')
        let login = new Login('manager', 'mobidevQA01!')
        login.writeLogin();
        login.writePass();
        login.clickLoginButton()
        expect(browser).toHaveUrlContaining('http://52.212.87.108/profile/1')
    });



})